resolvers += Resolver.bintrayRepo("allenai", "allenai-sbt-plugins")

addSbtPlugin("org.allenai.plugins" % "allenai-sbt-plugins" % "1.1.7")

addSbtPlugin("com.typesafe.sbt" % "sbt-native-packager" % "1.0.6")
