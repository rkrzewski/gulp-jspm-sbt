var angular = require("angular");
require("angular-websocket");

angular.module("app", ["ngWebSocket"]);
require("./app/index");
